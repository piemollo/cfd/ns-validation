from numpy import *
from pylab import *
import argparse
import numpy as np
import numpy.matlib as npm

''' ====== ====== Header  '''
print("#====== ====== ====== ====== ======#")
print("|     Numerical order in space     |")
print("#====== ====== ====== ====== ======#")
print("|");

''' ------ Read flags '''
parser = argparse.ArgumentParser( \
        description='This program displays results for space order', \
        epilog=' ');
parser.add_argument('--case', default='EthierSteinman', dest='name',\
        help='Case name (BercovierEngelman / EthierSteinman / \
        MinevEthier)');
args = parser.parse_args();
name = args.name
print("|  ### {} ###".format(name));

''' ------ ------ Direct '''
print("|  --- Direct solver")

''' ------ Absolut error '''
''' --- Load data '''
Err = loadtxt("./{}/results/SpaceDirectAbsError.dat".format(name));
h = Err[:,0]

f1=figure(1)
loglog(h,Err[:,1],'-*')
loglog(h,Err[:,2],'-*')
loglog(h,Err[:,3],'-*')

''' --- Compute order '''
Ord=[]
for i in range(3):
    ''' Fit '''
    Y = Err[:,1+i]
    p = polyfit(log10(h),log10(Y),1)
    Ord.append(p[0])
    ''' Plot '''
    Yy= polyval(p,log10(h))
    loglog(h,10**Yy,'k:')
    ''' Overlay '''
    xt = h[int(0.5*len(h))]
    yt = 10**Yy[int(0.5*len(h))] * 0.6
    text(xt,yt, r"$\alpha$={:.2f}".format(p[0]), fontsize=12)

''' --- Display '''
print("|  1) Absolut error :")
print("|      Velocity L^2 => {:.2f}".format(Ord[0]))
print("|      Velocity H^1 => {:.2f}".format(Ord[1]))
print("|      Pressure L^2 => {:.2f}".format(Ord[2]))

''' --- Graph overlay '''
title(" Error evolution \n Minev-Ethier (direct solver)")
xlabel("h (space step)")
ylabel("Error")
legend(["Velocity $L^2$"," Velocity $H^1$", "Pressure $L^2$"])
grid(linestyle=":")
f1.savefig("./{}/graph/SpaceDirectAbsCV.png".format(name))


''' ------ Relative error '''
''' --- Load data '''
Err = loadtxt("./{}/results/SpaceDirectRelError.dat".format(name));
h = Err[:,0]

f2=figure(2)
loglog(h,Err[:,1],'-*')
loglog(h,Err[:,2],'-*')
loglog(h,Err[:,3],'-*')

''' --- Compute order '''
Ord=[]
for i in range(3):
    ''' Fit '''
    Y = Err[:,1+i]
    p = polyfit(log10(h),log10(Y),1)
    Ord.append(p[0])
    ''' Plot '''
    Yy= polyval(p,log10(h))
    loglog(h,10**Yy,'k:')
    ''' Overlay '''
    xt = h[int(0.5*len(h))]
    yt = 10**Yy[int(0.5*len(h))] * 0.6
    text(xt,yt, r"$\alpha$={:.2f}".format(p[0]), fontsize=12)

print("|  2) Relative error :")
print("|      Velocity L^2 => {:.2f}".format(Ord[0]))
print("|      Velocity H^1 => {:.2f}".format(Ord[1]))
print("|      Pressure L^2 => {:.2f}".format(Ord[2]))

''' --- Graph overlay '''
title(" Relative error evolution \n Minev-Ethier (direct solver)")
xlabel("h (space step)")
ylabel("Error")
legend(["Velocity $L^2$","Velocity $H^1$","Pressure $L^2$"])
grid(linestyle=":")
f2.savefig("./{}/graph/SpaceDirectRelCV.png".format(name))

''' ------ ------ Uzawa '''
print("| --- Uzawa solver")

''' ------ Absolut error '''
''' --- Load data '''
Err = loadtxt("./{}/results/SpaceUzawaAbsError.dat".format(name));
h = Err[:,0]

f3=figure(3)
loglog(h,Err[:,1],'-*')
loglog(h,Err[:,2],'-*')
loglog(h,Err[:,3],'-*')

''' --- Compute order '''
Ord=[]
for i in range(3):
    ''' Fit '''
    Y = Err[:,1+i]
    p = polyfit(log10(h),log10(Y),1)
    Ord.append(p[0])
    ''' Plot '''
    Yy= polyval(p,log10(h))
    loglog(h,10**Yy,'k:')
    ''' Overlay '''
    xt = h[int(0.5*len(h))]
    yt = 10**Yy[int(0.5*len(h))] * 0.6
    text(xt,yt, r"$\alpha$={:.2f}".format(p[0]), fontsize=12)

''' --- Display '''
print("|  1) Absolut error :")
print("|      Velocity L^2 => {:.2f}".format(Ord[0]))
print("|      Velocity H^1 => {:.2f}".format(Ord[1]))
print("|      Pressure L^2 => {:.2f}".format(Ord[2]))

''' --- Graph overlay '''
title(" Error evolution \n Minev-Ethier (Uzawa solver)")
xlabel("h (space step)")
ylabel("Error")
legend(["Velocity $L^2$"," Velocity $H^1$", "Pressure $L^2$"])
grid(linestyle=":")
f3.savefig("./{}/graph/SpaceUzawaAbsCV.png".format(name))


''' ------ Relative error '''
''' --- Load data '''
Err = loadtxt("./{}/results/SpaceUzawaRelError.dat".format(name));
h = Err[:,0]

f4=figure(4)
loglog(h,Err[:,1],'-*')
loglog(h,Err[:,2],'-*')
loglog(h,Err[:,3],'-*')

''' --- Compute order '''
Ord=[]
for i in range(3):
    ''' Fit '''
    Y = Err[:,1+i]
    p = polyfit(log10(h),log10(Y),1)
    Ord.append(p[0])
    ''' Plot '''
    Yy= polyval(p,log10(h))
    loglog(h,10**Yy,'k:')
    ''' Overlay '''
    xt = h[int(0.5*len(h))]
    yt = 10**Yy[int(0.5*len(h))] * 0.6
    text(xt,yt, r"$\alpha$={:.2f}".format(p[0]), fontsize=12)

print("|  2) Relative error :")
print("|      Velocity L^2 => {:.2f}".format(Ord[0]))
print("|      Velocity H^1 => {:.2f}".format(Ord[1]))
print("|      Pressure L^2 => {:.2f}".format(Ord[2]))

''' --- Graph overlay '''
title(" Relative error evolution \n Minev-Ethier (Uzawa solver)")
xlabel("h (space step)")
ylabel("Error")
legend(["Velocity $L^2$","Velocity $H^1$","Pressure $L^2$"])
grid(linestyle=":")
f4.savefig("./{}/graph/SpaceUzawaRelCV.png".format(name))

''' ------ ------ Compare '''
''' ------ Relative error '''
''' --- Load data '''
DErr = loadtxt("./{}/results/SpaceDirectRelError.dat".format(name));
UErr = loadtxt("./{}/results/SpaceUzawaRelError.dat".format(name));
Dh = DErr[:,0]
Uh = UErr[:,0]

f5=figure(5)
loglog(Dh,DErr[:,1],'-o')
loglog(Dh,DErr[:,2],'-o')
loglog(Dh,DErr[:,3],'-o')
loglog(Uh,UErr[:,1],'--*')
loglog(Uh,UErr[:,2],'--*')
loglog(Uh,UErr[:,3],'--*')

''' --- Compute order '''
for i in range(3):
    ''' Fit '''
    Y = DErr[:,1+i]
    p = polyfit(log10(Dh),log10(Y),1)
    ''' Plot '''
    Yy= polyval(p,log10(Dh))
    loglog(Dh,10**Yy,'k:')
    ''' Overlay '''
    xt = Dh[int(0.5*len(Dh))]
    yt = 10**Yy[int(0.5*len(Dh))] * 0.6
    #text(xt,yt, r"$\alpha$={:.2f}".format(p[0]), fontsize=12)

for i in range(3):
    ''' Fit '''
    Y = UErr[:,1+i]
    p = polyfit(log10(Uh),log10(Y),1)
    ''' Plot '''
    Yy= polyval(p,log10(Uh))
    loglog(Uh,10**Yy,'k:')
    ''' Overlay '''
    xt = Uh[int(0.5*len(Uh))]
    yt = 10**Yy[int(0.5*len(Uh))] * 0.6
    #text(xt,yt, r"$\alpha$={:.2f}".format(p[0]), fontsize=12)

''' --- Graph overlay '''
title("Relative error evolution \n Minev-Ethier")
xlabel("h (space step)")
ylabel("Error")
legend(["Direct Vel. $L^2$","Direct Vel. $H^1$","Direct Pre. $L^2$", \
    "Uzawa Vel. $L^2$","Uzawa Vel. $H^1$","Uzawa Pre. $L^2$"])
grid(linestyle=":")
f5.savefig("./{}/graph/BothSpaceRelCV.png".format(name))

''' ====== ====== Compute time '''
print("|");
print("#====== ====== ====== ====== ======#")
print("|     Compared computation time    |")
print("#====== ====== ====== ====== ======#")
print("|");

''' ------ Get time '''
N = min(len(DErr[:,0]),len(UErr[:,0]))
T = zeros([N,3])

for i in range(N):
    T[i,0] = DErr[i,7]
    T[i,1] = DErr[i,4] + DErr[i,5]
    T[i,2] = UErr[i,4] + UErr[i,5]
    #T[i,1] = DErr[i,4]
    #T[i,2] = UErr[i,4]

f6=figure(6)
plot(T[:,0], T[:,1],"-*")
plot(T[:,0], T[:,2],"-*")

title(" Computation time \n Minev-Ethier")
legend(["Direct", "Uzawa"])
xlabel("Elem. number")
ylabel("Time (s)")

f7=figure(7)
loglog(T[:,0], T[:,1],"*-")
loglog(T[:,0], T[:,2],"*-")

''' ------ Fit '''
Y1 = T[:,1]
Y2 = T[:,2]
p1 = polyfit(log10(T[:,0]),log10(Y1),1)
p2 = polyfit(log10(T[:,0]),log10(Y2),1)
''' Plot '''
Yy1= polyval(p1,log10(T[:,0]))
Yy2= polyval(p2,log10(T[:,0]))
loglog(T[:,0],10**Yy1,'k:')
loglog(T[:,0],10**Yy2,'k:')
''' Overlay '''
xt1 = T[int(0.5*len(T[:,0])),0]
yt1 = 10**Yy1[int(0.5*len(T[:,0]))] * 0.6
text(xt1,yt1, r"$\alpha$={:.2f}".format(p1[0]), fontsize=12)

xt2 = T[int(0.5*len(T[:,0])),0]
yt2 = 10**Yy2[int(0.5*len(T[:,0]))] * 0.6
text(xt2,yt2, r"$\alpha$={:.2f}".format(p2[0]), fontsize=12)

title(" Computation time \n Minev-Ethier")
legend(["Direct", "Uzawa"])
xlabel("Elem. number (log)")
ylabel("Time (log)")

f6.savefig("./{}/graph/BothExecTime.png".format(name))
f7.savefig("./{}/graph/BothExecTimeLog.png".format(name))

''' ------ Time summary '''
NbIterD = DErr[0,6]
NbIterU = UErr[0,6]

TimePerIterD = DErr[:,5]/NbIterD
TimePerIterU = UErr[:,5]/NbIterU

print("#================================================#")
print("|                 Direct solver                  |")
print("#=========#==========#=============#=============#")
print("|  h step | Elem. nb | t init. (s) | t/iter (s)  |")
print("#=========#==========#=============#=============#")
for i in range(len(DErr[:,0])):
    print("|  {:5.2f}  | {:7d}  |   {:7.2f}   |   {:7.2f}   |"\
            .format(DErr[i,0], int(DErr[i,7]), DErr[i,4], \
            TimePerIterD[i]))
print("#=========#==========#=============#=============#")

print("\n#================================================#")
print("|                  Uzawa solver                  |")
print("#=========#==========#=============#=============#")
print("|  h step | Elem. nb | t init. (s) | t/iter (s)  |")
print("#=========#==========#=============#=============#")
for i in range(len(UErr[:,0])):
    print("|  {:5.2f}  | {:7d}  |   {:7.2f}   |   {:7.2f}   |"\
            .format(UErr[i,0], int(UErr[i,7]), UErr[i,4], \
            TimePerIterU[i]))
print("#=========#==========#=============#=============#")

show()
