//+
Point(1) = {1, -1, 1, 1.0};
//+
Point(2) = {1, -1, -1, 1.0};
//+
Point(3) = {1, 1, -1, 1.0};
//+
Point(4) = {1, 1, 1, 1.0};
//+
Point(5) = {-1, 1, 1, 1.0};
//+
Point(6) = {-1, 1, -1, 1.0};
//+
Point(7) = {-1, -1, -1, 1.0};
//+
Point(8) = {-1, -1, 1, 1.0};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Line(5) = {4, 5};
//+
Line(6) = {5, 6};
//+
Line(7) = {6, 7};
//+
Line(8) = {7, 8};
//+
Line(9) = {8, 5};
//+
Line(10) = {1, 8};
//+
Line(11) = {2, 7};
//+
Line(12) = {3, 6};
//+
Curve Loop(1) = {1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {10, 9, -5, 4};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {3, 5, 6, -12};
//+
Plane Surface(3) = {3};
//+
Curve Loop(4) = {11, -7, -12, -2};
//+
Plane Surface(4) = {4};
//+
Curve Loop(5) = {8, 9, 6, 7};
//+
Curve Loop(6) = {10, -8, -11, -1};
//+
Plane Surface(5) = {6};
//+
Plane Surface(6) = {5};
//+
Surface Loop(1) = {2, 5, 6, 3, 1, 4};
//+
Volume(1) = {1};
//+
Physical Surface("s0") = {1};
//+
Physical Surface("s1") = {2};
//+
Physical Surface("s2") = {3};
//+
Physical Surface("s3") = {4};
//+
Physical Surface("s4") = {5};
//+
Physical Surface("s5") = {6};
//+
Physical Volume("v0") = {1};
