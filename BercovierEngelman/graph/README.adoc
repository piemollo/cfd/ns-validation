Graphs from SpaceGraph.py/TimeGraph.py
Abs = absolut error
Rel = relative error

Theoretical convergence
[width="40%",frame="topbot",options="header,footer"]
|======================
|Space		| Alpha	
|Velocity (L2)	| 3	 
|Velocity (H1)	| 2	
|Pressure (L2)	| 2	
|======================
